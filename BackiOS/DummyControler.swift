
import UIKit


class DummyController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let label: UILabel
    
    init() {
        label = UILabel()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        label.text = "Возможность не поддерживается"
        label.textAlignment = .center
        view.addSubview(label)
        view.backgroundColor = UIColor.orange
        
        label.layer.shadowColor = #colorLiteral(red: 0.3943038313, green: 0.2569768011, blue: 0.1926086992, alpha: 1).cgColor
        label.layer.shadowOffset = CGSize(width: 0, height: 5)
        label.layer.shadowOpacity = 0.5
        label.layer.shadowRadius = 3.0
    }
    
    override func viewDidLayoutSubviews() {
        label.frame = view.bounds
    }
    
}

