import UIKit
import CoreImage
import Vision


@available(iOS 13.0, *)
struct Fridge {
    
    var masklevel: Int { didSet { tryGetMask() } }
    var viewport: SIMD2<Double>
    var background: URL?
    var foreground: URL? { didSet { tryGetMask() } }
    var mask: CIImage?

    
    
    func blurAdjust(image: CIImage, value: Float) -> CIImage {
        let filter = CIFilter(name:"CIBoxBlur")!
        filter.setValue(value, forKey: kCIInputRadiusKey)
        filter.setValue(image, forKey: kCIInputImageKey)
        return filter.outputImage!
    }
    
    func whiteAdjust(image: CIImage, value: Float) -> CIImage {

        let filter = CIFilter(name:"CIExposureAdjust")!
        filter.setValue(image, forKey: kCIInputImageKey)
        filter.setValue(value * 0.5, forKey: kCIInputEVKey)
        return filter.outputImage!
    }
    
    func getAdjustedImage(white: Float, blur: Float) throws -> CGImage {
        guard let mask = mask else { throw ObservateError.nomask }
        let adjmask = whiteAdjust(image: mask, value: white)
        let blurrmask = blurAdjust(image: adjmask, value: blur)
        return try composeResult(mask: blurrmask)
    }
    
    
    func composeResult(mask: CIImage) throws -> CGImage {
        
        var option: [CIImageOption: Any] = [:]
        option[.applyOrientationProperty] = true
        var mask = mask
        
        if let photo = foreground, let back = background {
            guard var forephoto = CIImage(contentsOf: photo, options: option) else { throw ObservateError.error }
            guard var backphoto = CIImage(contentsOf: back, options: option) else { throw ObservateError.error }
            
            var factor: Double
            
            if forephoto.extent.width > forephoto.extent.height {
                factor = viewport.y / Double(forephoto.extent.width)
            } else {                
                factor = viewport.x / Double(forephoto.extent.width)
            }
            
            var ihat: SIMD2<Double> = [factor, 0]
            var jhat: SIMD2<Double> = [0, factor]
            var matrix = CGAffineTransform(a: CGFloat(ihat.x), 
                                           b: CGFloat(ihat.y), 
                                           c: CGFloat(jhat.x), 
                                           d: CGFloat(jhat.y), 
                                           tx: 0, 
                                           ty: 0)
            forephoto = forephoto.transformed(by: matrix)
            mask = mask.transformed(by: matrix)
            
            if backphoto.extent.width > backphoto.extent.height {
                factor = viewport.y / Double(backphoto.extent.width) 
            } else {
                factor = viewport.x / Double(backphoto.extent.width)
            }
            
            ihat = [factor, 0]
            jhat = [0, factor]
            matrix = CGAffineTransform(a: CGFloat(ihat.x), 
                                       b: CGFloat(ihat.y), 
                                       c: CGFloat(jhat.x), 
                                       d: CGFloat(jhat.y), 
                                       tx: 0, 
                                       ty: 0)
            backphoto = backphoto.transformed(by: matrix) 
            
            let resizeFilter = CIFilter(name:"CIBlendWithMask")!
            resizeFilter.setValue(forephoto, forKey: kCIInputImageKey)
            resizeFilter.setValue(backphoto, forKey: kCIInputBackgroundImageKey)
            resizeFilter.setValue(mask, forKey: kCIInputMaskImageKey)
            guard let outputImage = resizeFilter.outputImage else { throw ObservateError.error }
            guard let cgimage = CIContext().createCGImage(outputImage, from: outputImage.extent) else { throw ObservateError.error }
            return cgimage
        } else {
            throw ObservateError.error
        }
        
        
    } 
    
    mutating func processImage() throws -> CGImage {
    
        var option: [CIImageOption: Any] = [:]
        option[.applyOrientationProperty] = true
        
        if let mask = mask, let _ = foreground, let _ = background {
            return try composeResult(mask: mask)
        } else if foreground != nil && background == nil {
            guard let photo = foreground else { throw ObservateError.error }
            guard let backphoto = CIImage(contentsOf: photo, options: option) else { throw ObservateError.error }
            guard let cgimage = CIContext().createCGImage(backphoto, from: backphoto.extent) else { throw ObservateError.error }
            return CIContext().createCGImage(mask!, from: backphoto.extent)! 
//            return cgimage
        } else if let back = background {
            guard let backphoto = CIImage(contentsOf: back, options: option) else { throw ObservateError.error }
            guard let cgimage = CIContext().createCGImage(backphoto, from: backphoto.extent) else { throw ObservateError.error }
            return cgimage
        } else {
            throw ObservateError.error
        }
        
    }
    
    mutating func tryGetMask() {
        do {
            try observate()
        } catch let error {
            print("error: \(error)")
        }
    }
    
    mutating func observate() throws {
        guard let back = foreground else { throw ObservateError.error }
        let handler = VNImageRequestHandler(url: back)
        let request = VNGenerateObjectnessBasedSaliencyImageRequest()
        try handler.perform([request])
        guard let result = request.results?.first else { throw ObservateError.error }
        guard let observation = result as? VNSaliencyImageObservation else { throw ObservateError.error }
        let pixelbuffer = observation.pixelBuffer
        BufferModifier().contrast(pixelBuffer: pixelbuffer, masklevel: masklevel)
        let processimage = CIImage(cvPixelBuffer: pixelbuffer) 
 
        
        // Desired output size
        let data = try! Data(contentsOf: back)
        let uiimage = UIImage(data: data)!
        let targetSize = uiimage.size
        
        // Compute scale and corrective aspect ratio
        let scale = targetSize.height / processimage.extent.height
        let aspectRatio = targetSize.width / targetSize.height
        
        // Apply resizing
        let resizeFilter = CIFilter(name:"CILanczosScaleTransform")!
        resizeFilter.setValue(processimage, forKey: kCIInputImageKey)
        resizeFilter.setValue(scale, forKey: kCIInputScaleKey)
        resizeFilter.setValue(aspectRatio, forKey: kCIInputAspectRatioKey)
        let outputImage = resizeFilter.outputImage
        mask = outputImage
        
//        mask = processimage // Стереть
        
        
    }
    
    
    enum ObservateError: Error {
        case error, nomask
    }
}


struct BufferModifier {
    func contrast(pixelBuffer: CVPixelBuffer, masklevel: Int) {
        
        CVPixelBufferLockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
        let bufferWidth = Int(CVPixelBufferGetWidth(pixelBuffer))
        let bufferHeight = Int(CVPixelBufferGetHeight(pixelBuffer))
        let bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuffer)
        
        let format = CVPixelBufferGetPixelFormatName(pixelBuffer: pixelBuffer)
        print("pixel: \(format)")
        
        
        guard let baseAddress = CVPixelBufferGetBaseAddress(pixelBuffer) else {
            return
        }
        

        var rowsfloat = [[Float32]]()
        
        
        for row in 0..<bufferHeight {
            

            var floats = [Float32]()
            
            var pixel = baseAddress + row * bytesPerRow
            
            for _ in 0..<bufferWidth {
                let blue = pixel
                let b = blue.load(as: Float32.self)
                let resultpixel: Float32
                let a = Float(masklevel) / 10
                if a < 0.05 {
                    pixel += 4;
                    continue 
                }
                if b < a { resultpixel = 0 }
                else { resultpixel = 1.0 }
                blue.storeBytes(of: resultpixel, as: Float32.self)
                let c = blue.load(as: Float32.self)
                floats.append(c)
                pixel += 4;
            }
            

            rowsfloat.append(floats)
        }
    
        CVPixelBufferUnlockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
        
    }
}
