//
//  ViewController.swift
//  BackiOS
//
//  Created by oktet on 26.11.2020.
//

import UIKit
import Files

@available(iOS 13.0, *)
class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    enum WhichPhotoIsGetting {
        enum Source { case camera, gellery }
        case background(Source)
        case foreground(Source)
        case none
        var fileneme: String {
            switch self {
            case .background: return "back"
            case .foreground: return "fore"
            default: return ""
            }
        }
    }
    
    let topButton: UIButton
    let bottomButton: UIButton
    let imageView: UIImageView    
    let sliderWhite: UISlider
    let sliderAlpha: UISlider
    let segments: UISegmentedControl
    var process: WhichPhotoIsGetting
    var fridge: Fridge
    
    init() {
        topButton = UIButton(type: .system)
        bottomButton = UIButton(type: .system)
        imageView = UIImageView()
        process = .none
        fridge = Fridge(masklevel: 7, viewport: .zero)
        sliderWhite = UISlider()
        sliderAlpha = UISlider()
        segments = UISegmentedControl()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.orange
        topButton.setTitle("Выбрать фото", for: .normal)
        bottomButton.setTitle("Выбрать фон", for: .normal)
        topButton.tintColor = UIColor.white
        bottomButton.tintColor = UIColor.white
        
        topButton.layer.shadowColor = #colorLiteral(red: 0.3943038313, green: 0.2569768011, blue: 0.1926086992, alpha: 1).cgColor
        topButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        topButton.layer.shadowOpacity = 0.5
        topButton.layer.shadowRadius = 3.0
        topButton.addTarget(self, action: #selector(Self.tapToTop), for: .touchUpInside)
        
        bottomButton.layer.shadowColor = #colorLiteral(red: 0.3943038313, green: 0.2569768011, blue: 0.1926086992, alpha: 1).cgColor
        bottomButton.layer.shadowOffset = CGSize(width: 0, height: 5)
        bottomButton.layer.shadowOpacity = 0.5
        bottomButton.layer.shadowRadius = 3.0
        bottomButton.addTarget(self, action: #selector(Self.tapToBottom), for: .touchUpInside)
        
        var color = UIColor.white
        color = color.withAlphaComponent(0.1)
        imageView.backgroundColor = color
        imageView.contentMode = .scaleAspectFit
//        imageView.contentMode = .center
        
        let colorsl = #colorLiteral(red: 0.98, green: 0.49, blue: 0, alpha: 1)
        sliderAlpha.backgroundColor = colorsl
        sliderWhite.backgroundColor = colorsl
        
        let maxc = #colorLiteral(red: 1, green: 0.8601315217, blue: 0.08545883141, alpha: 1)
        let minc = #colorLiteral(red: 0.9424111774, green: 0.4737115938, blue: 0, alpha: 1)
        sliderAlpha.maximumTrackTintColor = minc
        sliderWhite.maximumTrackTintColor = minc
        sliderAlpha.minimumTrackTintColor = maxc
        sliderWhite.minimumTrackTintColor = maxc
        
        sliderWhite.addTarget(self, action: #selector(Self.tapslider(sender:)), for: .valueChanged)
        sliderAlpha.addTarget(self, action: #selector(Self.bottomslider(sender:)), for: .valueChanged)
        
        sliderAlpha.minimumValue = 0
        sliderAlpha.maximumValue = 150
       
        for item in (0...10).reversed() {
            segments.insertSegment(withTitle: "\(item)", at: 0, animated: false)
        }
        segments.selectedSegmentIndex = fridge.masklevel
        segments.addTarget(self, action: #selector(Self.tapsegment(sender:)), for: .valueChanged)
        
        view.addSubview(topButton)
        view.addSubview(bottomButton)
        view.addSubview(imageView)
        view.addSubview(sliderWhite)
        view.addSubview(sliderAlpha)
        view.addSubview(segments)
        
        UserDefaults.standard.set(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        print("loaded")
    }

    override func viewDidLayoutSubviews() {
        let diagonal: SIMD2<Double> = [Double(view.bounds.width), Double(view.bounds.height)]
        let partcount = 7
        let part = diagonal / Double(partcount)
        let buttonDiagonal: SIMD2<Double> = [diagonal.x, part.y]
        let topOrigin: SIMD2<Double> = [0, 0]
        let bottomOrigin: SIMD2<Double> = [0, part.y * Double(partcount - 1)]
        let topframe = CGRect(origin: topOrigin.cgpoint, size: CGSize(width: buttonDiagonal.x, height: buttonDiagonal.y))
        let bottomFrame = CGRect(origin: bottomOrigin.cgpoint, size: CGSize(width: buttonDiagonal.x, height: buttonDiagonal.y))
        topButton.frame = topframe
        bottomButton.frame = bottomFrame
        
        let controls: Double = 3
        
        var sliderorigin: SIMD2<Double> = [0, part.y + 1]
        var sliderframe = CGRect(origin: sliderorigin.cgpoint, size: CGSize(width: buttonDiagonal.x, height: buttonDiagonal.y / controls))
        sliderWhite.frame = sliderframe
        
        sliderorigin = [0, part.y + part.y / controls + 2]
        sliderframe = CGRect(origin: sliderorigin.cgpoint, size: CGSize(width: buttonDiagonal.x, height: buttonDiagonal.y / controls))
        sliderAlpha.frame = sliderframe
        
        sliderorigin = [0, part.y + part.y / controls + part.y / controls + 2]
        sliderframe = CGRect(origin: sliderorigin.cgpoint, size: CGSize(width: buttonDiagonal.x, height: buttonDiagonal.y / controls))
        segments.frame = sliderframe
        
        let imageorigin = CGPoint(x: 0, y: part.y * 2)
        let imagesize = CGSize(width: diagonal.x, height: part.y * Double(partcount - 3))
        let imageframe = CGRect(origin: imageorigin, size: imagesize)
        imageView.frame = imageframe
        imageView.clipsToBounds = true
        fridge.viewport = diagonal
    }
    
    
    @objc func tapToTop() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: { action in
            self.process = .foreground(.camera)
            self.getPhoto()
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { action in
            self.process = .foreground(.gellery)
            self.getPhoto()
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: { action in
            alert.dismiss(animated: true, completion: nil)
        }))
        present(alert, animated: true, completion: nil)
        
    }
    
    @objc func tapToBottom() { 
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Камера", style: .default, handler: { action in
            self.process = .background(.camera)
            self.getPhoto()
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { action in
            self.process = .background(.gellery)
            self.getPhoto()
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: { action in
            alert.dismiss(animated: true, completion: nil)
        }))
        present(alert, animated: true, completion: nil)
    }
    

    
    func getPhoto() {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        switch process {
        case .background(let source):
            switch source {
            case .camera: pickerController.sourceType = .camera
            case .gellery: pickerController.sourceType = .photoLibrary
            }
        case .foreground(let source):
            switch source {
            case .camera: pickerController.sourceType = .camera
            case .gellery: pickerController.sourceType = .photoLibrary
            }
        default: break
        }
        
        show(pickerController, sender: self)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, 
                                        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
//        guard let image = info[.originalImage] as? UIImage else {  print("no image"); return }
//        imageView.image = image
//        print("info: \(info)")
    
        var urlone: URL?
        
        switch process {
        case .background(.camera), .foreground(.camera):
            guard let image = info[.originalImage] as? UIImage else { break }
            guard let fixed = image.fixedOrientation() else { break }
            guard let data = fixed.pngData() else { break }
            do {
                let folder = try Folder.temporary.createSubfolder(named: "Images")
                let file = try folder.createFile(named: "image\(process.fileneme)")
                try file.write(data)
                urlone = file.url
            } catch {
                break
            }
        default:
            guard let z = info[.imageURL] as? URL else { break }
            urlone = z
        }
        
        
        
        guard let url = urlone else {  print("no url"); return }
        
        switch process {
        case .foreground: fridge.foreground = url
        case .background: fridge.background = url
        case .none: break
        }
        
  
        
        
        do {
            let image = try fridge.processImage()
            imageView.image = UIImage(cgImage: image)
        } catch let error {
            print("error 3: \(error)")
        }
        
        process = .none
        picker.dismiss(animated: true)
    }
    
    @objc func tapsegment(sender: UISegmentedControl) {
        fridge.masklevel = Int(sender.selectedSegmentIndex)
        do {            
            let image = try fridge.getAdjustedImage(white: sliderWhite.value, blur: sliderAlpha.value)
            imageView.image = UIImage(cgImage: image)
        } catch {
            
            do {
                let image = try fridge.processImage()
                imageView.image = UIImage(cgImage: image)
            } catch {
                print("segment no image")
            }
            
        }
    }
    @objc func tapslider(sender: UISlider) {
        adjustmask()
    }
    @objc func bottomslider(sender: UISlider) {
        adjustmask()
    }
    
    func adjustmask() {
        do {            
            let image = try fridge.getAdjustedImage(white: sliderWhite.value, blur: sliderAlpha.value)
            imageView.image = UIImage(cgImage: image)
        } catch {
            print("no image")
        }
    }
}

