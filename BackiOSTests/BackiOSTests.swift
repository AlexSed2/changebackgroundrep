//
//  BackiOSTests.swift
//  BackiOSTests
//
//  Created by oktet on 26.11.2020.
//

class ValueQuestion: XCTestCase  {
    
    class Storage {
        var item: String = ""
        func upload(item: String) { self.item = item }
        func download(key: String) -> String { item }
    }
    
    struct StackOnDisk {
        var storage: Storage
        init() {
            storage = Storage()
        }
        mutating func push(item: String) { 
            if !isKnownUniquelyReferenced(&storage) {
                // We're sharing `storage`, make a copy
                storage = Storage()
            }
            
            storage.upload(item: item) 
        }
        mutating func pop(key: String) -> String { storage.download(key: key) }
    }
    
    class Controller {
        func work() {
            var stackone = StackOnDisk()
            stackone.push(item: "Maya")
            var stacktwo = stackone // How to initialize new Storage at stacktwo instance at this moment
            stacktwo.push(item: "Willy")
            let firstitem = stackone.pop(key: "key")
            XCTAssert(firstitem == "Maya")
            print(firstitem)
        }
    }

    func test() {
        Controller().work() // prints Whilly
    }
    
}



import XCTest
@testable import BackiOS

class BackiOSTests: XCTestCase {
    
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
